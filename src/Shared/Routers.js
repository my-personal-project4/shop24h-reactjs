import { useSelector} from "react-redux";
import { Route, Routes } from "react-router";
import CartPage from "../Screens/Cart/CartPage";
import CheckOutPage from "../Screens/CheckOut/CheckOutPage";
import { Home } from "../Screens/Home/Home";
import LoginPage from "../Screens/Login/LoginPage";
import MyOrdersPage from "../Screens/MyOrders/MyOrdersPage";
import ProductItem from "../Screens/ProductItem/ProductItem";
import { About } from "../Screens/About/About";
import { Contact } from "../Screens/Contact/Contact";
import ProductScreen  from "../Screens/ProductList/ProductScreen";
import SignUp from "../Screens/SignUp/SignUp";

export const AllRouters = () => {
  const {productInfo} = useSelector(reduxData => reduxData.getProductsReducer);
  return ( 
    <Routes>
      <Route path="/" element={<Home />}></Route>
      <Route path="/login" element={<LoginPage />}></Route>
      <Route path="/signUp" element={<SignUp />}></Route>
      <Route path="/Contact" element={<Contact />}></Route>
      <Route path="About" element={<About />}></Route>
      <Route
        path="/products"
        element={<ProductScreen></ProductScreen>}
      ></Route>
      <Route
        path="/products/:productid"
        element={<ProductItem productInfo={productInfo} />}
      ></Route>
      <Route path="/Cart" element={<CartPage />}></Route>
      <Route path="/CheckOut" element={<CheckOutPage />}></Route>
      <Route path="/myOrders" element={<MyOrdersPage />}></Route>
    </Routes>
  );
};
