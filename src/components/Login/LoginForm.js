import { Grid, Typography, TextField, Button, Container } from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  userLoginAction,
  emailLoginAction,
} from "../../Redux/Actions/UserLoginAction";
// import "./Login.css";
import {
  GoogleAuthProvider,
  signInWithPopup,
  signInWithEmailAndPassword,
} from "firebase/auth";
// import {useEffect} from "react";
import auth from "../../firebase";

const provider = new GoogleAuthProvider();

function LogInForm() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { emailLogin } = useSelector((reduxData) => reduxData.userLoginReducer);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const {user} = useSelector(reduxData=> reduxData.userLoginReducer);
  // console.log(user);
  const signUpWithEmail = () => {
    navigate("/signUp");
  };

  async function logInGoogle() {
    await signInWithPopup(auth, provider).then(
      async (result) => {
        dispatch(userLoginAction(result));
        // console.log(result)
        //3 - pick the result and store the token
        const token = await auth?.currentUser?.getIdToken(true);
        //4 - check if have token in the current user
        if (token) {
          //5 - put the token at localStorage (We'll use this to make requests)
          localStorage.setItem("@token", token);
          //6 - navigate user to the book list
          navigate("/");
        }
      },
      function (error) {
        console.log(error);
      }
    );
  }

  async function logInWithEmail() {
    await signInWithEmailAndPassword(auth, email, password).then(
      async (result) => {
        // console.log(result)
        dispatch(emailLoginAction(result));
        //3 - pick the result and store the token
        const token = await auth?.currentUser?.getIdToken(true);
        //4 - check if have token in the current user
        if (token) {
          //5 - put the token at localStorage (We'll use this to make requests)
          localStorage.setItem("@token", token);
          //6 - navigate user to the book list
          navigate("/");
        }
      },
      function (error) {
        console.log(error);
      }
    );
  }
  // const logInWithEmail = () => {
  //   signInWithEmailAndPassword(auth, email, password)
  //     .then((userCredential) => {
  //       // Signed in
  //       alert("đăng nhập thành công");
  //       console.log(userCredential);
  //       // ...
  //     })
  //     .catch((error) => {
  //       alert("đăng nhập thất bại. Email hoặc mật khẩu không đúng")
  //       console.log(error);
  //     });
  // }

  return (
    <div
      style={{
        background:
          "linear-gradient(326deg, rgba(223,167,33,0.9612219887955182) 6%, rgba(63,215,251,1) 60%)",
      }}
    >
      <Container>
        <Grid
          container
          sx={{ justifyContent: "space-evenly", alignItems: "center" }}
        >
          <Grid
            item
            xs={12}
            lg={6}
            md={8}
            sm={12}
            padding={3}
            sx={{
              borderRadius: "10px",
              background:
                "linear-gradient(326deg, rgba(223,167,33,0.9612219887955182) 6%, rgba(63,215,251,1) 60%)",
              boxShadow: "0 8px 32px 0 rgba(31, 38, 135, 0.37)",
              backdropFilter: "blur(8.5px)",
            }}
          >
            <Grid container textAlign="center">
              <Grid item xs={12} lg={12} md={12} sm={12} mt={3}>
                <Typography variant="h4" gutterBottom component="div">
                  Welcome Back COZA Store!
                </Typography>
              </Grid>
              <Grid item xs={12} lg={12} md={12} sm={12} mt={5} padding={1}>
                <TextField
                  label="Email Address *"
                  variant="outlined"
                  fullWidth
                  value={email}
                  onChange={(event) => {
                    setEmail(event.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} lg={12} md={12} sm={12} mt={3} padding={1}>
                <TextField
                  type="password"
                  label="Password *"
                  variant="outlined"
                  fullWidth
                  value={password}
                  onChange={(event) => {
                    setPassword(event.target.value);
                  }}
                />
              </Grid>
              <Grid container>
                <Grid
                  item
                  xs={12}
                  lg={6}
                  md={6}
                  sm={12}
                  mt={3}
                  mb={5}
                  padding={1}
                >
                  <Button
                    onClick={logInWithEmail}
                    sx={{
                      borderRadius: "30px",
                      background:
                        "linear-gradient(to right, #56C1E1 0%, #35A9CE 50%))",
                    }}
                    variant="contained"
                    size="large"
                    fullWidth
                  >
                    LOG IN
                  </Button>
                </Grid>
                <Grid
                  item
                  xs={12}
                  lg={6}
                  md={6}
                  sm={12}
                  mt={3}
                  mb={5}
                  padding={1}
                >
                  <Button
                    onClick={signUpWithEmail}
                    sx={{
                      borderRadius: "30px",
                      background:
                        "linear-gradient(to right, #56C1E1 0%, #35A9CE 50%))",
                    }}
                    variant="contained"
                    size="large"
                    fullWidth
                  >
                    SIGN UP
                  </Button>
                </Grid>
              </Grid>
              <Grid item xs={12} lg={12} md={12} sm={12} mt={3}>
                <Typography variant="h4" gutterBottom component="h5">
                  Or Sign In With
                </Typography>
              </Grid>
              <Grid container>
                <Grid
                  item
                  xs={12}
                  lg={12}
                  md={12}
                  sm={12}
                  mt={3}
                  mb={5}
                  padding={1}
                >
                  <Button
                    sx={{
                      borderRadius: "30px",
                      background:
                        "linear-gradient(326deg, rgba(223,33,33,0.9612219887955182) 51%, rgba(251,85,63,1) 96%)",
                    }}
                    onClick={logInGoogle}
                    variant="contained"
                    size="large"
                    fullWidth
                  >
                    Google
                  </Button>
                </Grid>
                {/* <Grid item xs={6} lg={6} md={6} sm={6} mt={3} mb={5} padding={1}>
                          <Button sx={{borderRadius: "30px", background: "linear-gradient(to right, #14163c 0%, #03217b 79%)"}} variant="contained" size="large" fullWidth>Phone</Button>
                      </Grid> */}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default LogInForm;
