import {
  Grid,
  Typography,
  TextField,
  Button,
  Modal,
  Box,
  Container,
} from "@mui/material";
import { useState } from "react";
// import { useDispatch, useSelector} from "react-redux";
// import { userLoginAction } from "../../Redux/Actions/UserLoginAction";
import { createUserWithEmailAndPassword } from "firebase/auth";
import {useNavigate } from "react-router-dom";
import auth from "../../firebase";
// import { emailLoginAction } from "../../Redux/Actions/UserLoginAction";
// import { useDispatch } from "react-redux";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function SignUpForm() {
  // const dispatch = useDispatch()
  const navigate = useNavigate()
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [openModal, setOpenModal] = useState(false);

  const signUpWithEmail = () => {
    createUserWithEmailAndPassword(auth, email, password)
      .then((result) => {
        setOpenModal(true);
       
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const loginButton = () => {
    navigate('/login')
  }
  const handleClose = () => {
    setOpenModal(false);
  };
  return (
    <>
      <div
        style={{
          background:
            "linear-gradient(326deg, rgba(223,167,33,0.9612219887955182) 6%, rgba(63,215,251,1) 60%)",
        }}
      >
        <Container>
          <Grid
            container
            sx={{ justifyContent: "space-evenly", alignItems: "center" }}
          >
            <Grid
              item
              xs={6}
              lg={6}
              md={8}
              sm={12}
              padding={3}
              sx={{
                borderRadius: "10px",
                background:
                  "linear-gradient(326deg, rgba(223,167,33,0.9612219887955182) 6%, rgba(63,215,251,1) 60%)",
                boxShadow: "0 8px 32px 0 rgba(31, 38, 135, 0.37)",
                backdropFilter: "blur(8.5px)",
              }}
            >
              <Grid container textAlign="center">
                <Grid item xs={12} lg={12} md={12} sm={12} mt={3}>
                  <Typography variant="h4" gutterBottom component="div">
                    Sign Up For Free
                  </Typography>
                </Grid>
                <Grid item xs={12} lg={12} md={12} sm={12} mt={3} padding={1}>
                  <TextField
                    label="Email Address *"
                    variant="outlined"
                    fullWidth
                    value={email}
                    onChange={(event) => {
                      setEmail(event.target.value);
                    }}
                  />
                </Grid>
                <Grid item xs={12} lg={12} md={12} sm={12} mt={3} padding={1}>
                  <TextField
                    type="password"
                    label="Set A Password *"
                    variant="outlined"
                    value={password}
                    fullWidth
                    onChange={(event) => {
                      setPassword(event.target.value);
                    }}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  lg={12}
                  md={12}
                  sm={12}
                  mt={3}
                  mb={5}
                  padding={1}
                >
                  <Button
                    onClick={signUpWithEmail}
                    sx={{
                      borderRadius: "30px",
                      background:
                        "linear-gradient(to right, #14163c 0%, #03217b 79%)",
                    }}
                    variant="contained"
                    size="large"
                    fullWidth
                  >
                    SIGN UP
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </div>
      <Modal
        open={openModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Sign Up successfully!!
          </Typography>
          <Button variant="outlined" onClick={loginButton}>Login</Button>
          {/* <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                </Typography> */}
        </Box>
      </Modal>
    </>
  );
}

export default SignUpForm;
