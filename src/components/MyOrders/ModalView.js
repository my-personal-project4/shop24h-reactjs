import {
  Grid,
  Alert,
  Box,
  Button,
  TextField,
  FormControl,
  InputLabel,
  Modal,
  Select,
  Snackbar,
  Typography,
  MenuItem,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
} from "@mui/material";
// import { useState, useEffect } from 'react';
// import { useDispatch, useSelector } from "react-redux";
const styleNew = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "65%",
  bgcolor: "white",
  border: "2px solid #000",
  boxShadow: 24,
  borderRadius: "15px",
  fontWeight: "bold",
};
function ModalView(props) {
  const onBtnCancelClick = () => {
    props.handleCloseView();
  };

  return (
    <>
      <Modal
        open={props.openModalView}
        onClose={props.handleCloseView}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={styleNew}>
          <Typography
            id="modal-modal-title"
            variant="h4"
            align="center"
            style={{ color: "#00695c" }}
          >
            <strong>View Order Detail</strong>
          </Typography>

          <TableContainer sx={{marginTop: 5}}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="center">Image</TableCell>
                  <TableCell align="center">Product Name</TableCell>
                  <TableCell align="center">Product Type</TableCell>
                  <TableCell align="center">Price</TableCell>
                  <TableCell align="center">Quantity</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(props.dataView).length > 0
                  ? props.dataView.orderDetails.map((orderDetail, index) => {
                      return (
                        <TableRow key={index}>
                          <TableCell align="center">
                            <img
                              src={orderDetail.product.imageUrl}
                              width="50vW"
                              alt=""
                            ></img>
                          </TableCell>
                          <TableCell align="center">
                            {orderDetail.product.name}
                          </TableCell>
                          <TableCell align="center">
                            {orderDetail.product.type.name}
                          </TableCell>
                          <TableCell align="center">
                            {orderDetail.product.buyPrice}
                          </TableCell>
                          <TableCell align="center">
                            {orderDetail.quantity}
                          </TableCell>
                        </TableRow>
                      );
                    })
                  : null}
              </TableBody>
            </Table>
          </TableContainer>

          <Grid container className="mt-4 text-center">
            <Grid item sm={12}>
              <Grid container className="mt-4">
                <Grid item sm={6}></Grid>
                <Grid sx={{marginTop: 5}} item sm={6}>
                  <Button
                    onClick={onBtnCancelClick}
                    variant="contained"
                    color="error"
                  >
                    Cancel
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </>
  );
}

export default ModalView;
