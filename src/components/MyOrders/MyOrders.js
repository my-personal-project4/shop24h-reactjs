import React, { useState, useEffect } from "react";
import moment from "moment";
import {
  Grid,
  Pagination,
  Button,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  InputLabel,
  Select,
  MenuItem,
  Typography,
  Box,
  Modal,
  FormControl,
  TextareaAutosize,
  Snackbar,
  Alert,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import Rating from "@mui/material/Rating";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import Stack from "@mui/material/Stack";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  changePagePaginationOrder,
  getOrders,
} from "../../Redux/Actions/getOrdersAction";
import ModalView from "./ModalView";
import { createReview } from "../../Redux/Actions/reviewAction";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ReviewsIcon from "@mui/icons-material/Reviews";
import { Container } from "@mui/system";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "30%",
  bgcolor: "white",
  border: "2px solid #000",
  boxShadow: 24,
  borderRadius: "15px",
  fontWeight: "bold",
};
const StyledRating = styled(Rating)({
  "& .MuiRating-iconFilled": {
    color: "#ff6d75",
  },
  "& .MuiRating-iconHover": {
    color: "#ff3d47",
  },
});

const MyOrders = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { currentPage, orders, noPage } = useSelector((reduxData) => {
    return reduxData.getOrderReducers;
  });
  // console.log(orders)
  const [varRefeshPage, setVarRefeshPage] = useState(0);
  const [stars, setStars] = useState(0);
  const [feedback, setFeedback] = useState("");
  const [limitOrder, setLimitOrder] = useState(5);
  const [openModalReview, setOpenModalReview] = useState(false);
  const [openModalView, setOpenModalView] = useState(false);
  const handleCloseReview = () => setOpenModalReview(false);
  const handleCloseView = () => setOpenModalView(false);
  const [dataReview, setDataReview] = useState("");
  const [dataView, setDataView] = useState([]);
  const [openAlert, setOpenAlert] = useState(false);
  const [statusModal, setStatusModal] = useState("error");
  const [noidungAlertValid, setNoidungAlertValid] = useState("");
  const [productReview, setProductReview] = useState("ALL");

  const onChangePagination = (event, value) => {
    dispatch(changePagePaginationOrder(value));
    setVarRefeshPage(varRefeshPage + 1);
  };

  let token = localStorage.getItem("@token");

  const onBtnReviewClick = (data) => {
    setOpenModalReview(true);
    // dispatch(addCartItem(data))
    // console.log(data)
    setDataReview(data);
  };

  const onBtnViewClick = (data) => {
    setOpenModalView(true);
    setDataView(data);
  };
  const onBtnReviewAdd = () => {
    if (valiDate()) {
      dispatch(
        createReview(
          productReview,
          stars,
          feedback,
          setOpenAlert,
          setStatusModal,
          setNoidungAlertValid,
          dataReview,
          setOpenModalReview
        )
      );
    }
  };
  function formatCash(cash) {
    if (cash) return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const onBtnCancelClick = () => {
    handleCloseReview();
  };

  const goShoppingClick = () => {
    navigate("/products");
  };

  //Đóng Alert
  const handelCloseAlert = () => {
    setOpenAlert(false);
  };
  const valiDate = () => {
    if (productReview === "ALL") {
      setOpenAlert(true);
      setStatusModal("error");
      setNoidungAlertValid("Please select product!");
      return false;
    }
    if (stars === 0) {
      setOpenAlert(true);
      setStatusModal("error");
      setNoidungAlertValid("Please give a heart!");
      return false;
    }

    if (feedback === "") {
      setOpenAlert(true);
      setStatusModal("error");
      setNoidungAlertValid("Please give feedback!");
      return false;
    }
    return true;
  };
  useEffect(() => {
    dispatch(getOrders(currentPage, limitOrder));
  }, [currentPage, varRefeshPage, token, dataView]);

  return (
    <Container>
      {orders.length > 0 ? (
        <>
          <Grid container mt={5} mb={1}>
            <Grid item xs={4}>
              <Grid item container xs={12} justifyContent={"center"}>
                <Typography
                  variant="h5"
                  sx={{
                    display: "inline",
                    color: "orange",
                    fontWeight: "bold",
                  }}
                >
                  List Orders
                </Typography>
              </Grid>
            </Grid>

            <Grid item xs={4}>
              <Grid container justifyContent={"flex-end"}>
                <Grid item marginY={"auto"} mr={1}>
                  <InputLabel>Show orders</InputLabel>
                </Grid>
                <Grid item>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={limitOrder}
                    size="small"
                    onChange={(e) => setLimitOrder(e.target.value)}
                  >
                    <MenuItem value={5}>5</MenuItem>
                    <MenuItem value={10}>10</MenuItem>
                    <MenuItem value={25}>25</MenuItem>
                    <MenuItem value={50}>50</MenuItem>
                  </Select>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="center">Order ID</TableCell>
                  <TableCell align="center">OrderDate</TableCell>
                  <TableCell align="center">ShippedDate</TableCell>
                  <TableCell align="center">Status</TableCell>
                  <TableCell align="center">Total Cost</TableCell>
                  {/* <TableCell align="center">Note</TableCell> */}
                  {/* <TableCell align="center">OrderDetails</TableCell> */}
                  <TableCell align="center">View/Review</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {orders.length > 0
                  ? orders.map((orderItem, index) => {
                      return orderItem.orders.map((orderDetail, index1) => {
                        return (
                          <TableRow key={index1}>
                            <TableCell align="center">
                              {orderDetail.orderCode}
                            </TableCell>
                            <TableCell align="center">
                              {moment(orderDetail.orderDate).format(
                                "MM/DD/YYYY"
                              )}
                            </TableCell>
                            <TableCell align="center">
                              {moment(orderDetail.shippedDate).format(
                                "MM/DD/YYYY"
                              )}
                            </TableCell>
                            <TableCell align="center">
                              {orderDetail.statusOrder}
                            </TableCell>
                            <TableCell align="center">
                              {formatCash(orderDetail.cost)} $
                            </TableCell>
                            {/* <TableCell align="center">
                              {orderDetail.note}
                            </TableCell> */}
                            {/* <TableCell align="center">
                              <Button
                                onClick={() => {
                                  onBtnViewClick(orderDetail);
                                }}
                                
                              >
                                View Order Detail
                              </Button>
                            </TableCell> */}
                            {/* <TableCell align="center">
                              {orderDetail.orderDetails.map((value, index) => {
                                return (
                                  <div key={index}>
                                    <p>{"Sản phẩm: " + value.product.name}</p>
                                    <p>{"Số lượng: " + value.quantity}</p>
                                    <br></br>
                                  </div>
                                );
                              })}
                            </TableCell> */}
                            <TableCell align="center">
                              <VisibilityIcon
                                style={{
                                  cursor: "pointer",
                                  marginRight: 10,
                                  color: "green",
                                }}
                                onClick={() => {
                                  onBtnViewClick(orderDetail);
                                }}
                              ></VisibilityIcon>
                              <ReviewsIcon
                                style={{ cursor: "pointer", color: "orange" }}
                                onClick={() => {
                                  onBtnReviewClick(orderDetail);
                                }}
                              ></ReviewsIcon>
                              {/* <Button
                                onClick={() => {
                                  onBtnReviewClick(orderDetail);
                                }}
                                className="bg-success w-50"
                              >
                                Review
                              </Button> */}
                            </TableCell>
                          </TableRow>
                        );
                      });
                    })
                  : null}
              </TableBody>
            </Table>
          </TableContainer>
          <Grid container mt={3} mb={2} justifyContent="flex-end">
            <Grid item>
              <Pagination
                count={noPage}
                color="primary"
                defaultPage={1}
                onChange={onChangePagination}
              />
            </Grid>
          </Grid>
          <Modal
            open={openModalReview}
            onClose={handleCloseReview}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <Typography
                id="modal-modal-title"
                variant="h4"
                align="center"
                style={{ color: "#00695c" }}
              >
                <strong>Review Product</strong>
              </Typography>

              <Grid container style={{ marginTop: "30px" }}>
                <Grid item xs={6}>
                  <label
                    style={{ marginLeft: "10px" }}
                    id="demo-simple-select-label"
                  >
                    Product
                  </label>
                </Grid>
                <Grid mt={2} item xs={6} md={12} sm={12} lg={12}>
                  <Select
                    value={productReview}
                    onChange={(e) => setProductReview(e.target.value)}
                    fullWidth
                  >
                    <MenuItem value="ALL">Choose your product</MenuItem>
                    {dataReview
                      ? dataReview.orderDetails.map((element, index) => {
                          return (
                            <MenuItem key={index} value={element.product._id}>
                              {element.product.name}
                            </MenuItem>
                          );
                        })
                      : null}
                  </Select>
                </Grid>
              </Grid>

              <Grid container mt={5}>
                <Grid item xs={6}>
                  <label style={{ marginLeft: "10px" }}>Give A Heart</label>
                </Grid>
                <Grid item xs={6}>
                  <Stack spacing={1}>
                    <StyledRating
                      icon={<FavoriteIcon fontSize="inherit" />}
                      emptyIcon={<FavoriteBorderIcon fontSize="inherit" />}
                      value={stars}
                      onChange={(e) => setStars(parseInt(e.target.value))}
                    />
                  </Stack>
                </Grid>
              </Grid>
              <Grid container mt={5}>
                <Grid item xs={6}>
                  <label style={{ marginLeft: "10px" }}>Give Feedback</label>
                </Grid>
                <Grid item xs={6}>
                  <TextareaAutosize
                    aria-label="minimum height"
                    minRows={3}
                    style={{ width: 200, border: "solid 1px beige" }}
                    value={feedback}
                    onChange={(e) => setFeedback(e.target.value)}
                  />
                </Grid>
              </Grid>

              <Grid style={{ marginLeft: "10px" }} container mt={4}>
                <Grid item sm={12}>
                  <Grid container className="mt-4">
                    <Grid item sm={6}>
                      <Button
                        onClick={onBtnReviewAdd}
                        variant="contained"
                        color="warning"
                      >
                        Review
                      </Button>
                    </Grid>
                    <Grid item sm={6}>
                      <Button
                        onClick={onBtnCancelClick}
                        variant="contained"
                        color="error"
                      >
                        Cancel
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Modal>
          <ModalView
            openModalView={openModalView}
            setOpenModalView={setOpenModalView}
            dataView={dataView}
            handleCloseView={handleCloseView}
          />
          <Snackbar
            open={openAlert}
            autoHideDuration={6000}
            onClose={handelCloseAlert}
          >
            <Alert
              onClose={handelCloseAlert}
              severity={statusModal}
              sx={{ width: "100%" }}
            >
              {noidungAlertValid}
            </Alert>
          </Snackbar>
        </>
      ) : (
        <Typography
          variant="h5"
          sx={{
            display: "inline",
            marginRight: "15px",
            color: "orange",
            fontWeight: "bold",
            // height: "500px"
          }}
        >
          You don't have any orders
        </Typography>
      )}
      <Button variant="outlined" onClick={goShoppingClick}>
        Go Shopping now!!!
      </Button>
    </Container>
  );
};

export default MyOrders;
