import { Container, Grid, Pagination } from "@mui/material";
import React, { useEffect} from "react";
import BreadCrumb from '../BreadCrumb/BreadCrumb';
import  ProductCard  from "./ProductCard";
import  ProductFilter  from "./ProductFilter";
import { useDispatch, useSelector } from "react-redux";
import { changePagePagination, getAllProductFilter} from "../../Redux/Actions/GetAllProductAction";


function ProductList () {
  const dispatch = useDispatch();
  const {products, noPage, currentPage} = useSelector((reduxData) => reduxData.getProductsReducer);
  const {type} = useSelector(reduxData=> reduxData.filterValueReducer);
  const {productName, minPrice, maxPrice, color, size} = useSelector(reduxData=> reduxData.filterValueReducer);
  const breadcrumbArray = [
    {
      name: "Home",
      url: "/",
    },
  ];

  useEffect(() => {
    dispatch(getAllProductFilter(productName, minPrice, maxPrice,type, color, size, currentPage));
  }, [currentPage]);

  const onChangePagination = (event, value) => {
    dispatch(changePagePagination(value));
  }

  return(
    <>
      <Container>
          <BreadCrumb breadcrumbArray={breadcrumbArray} pageName="All Products" />
      </Container>
      <Grid container>
        <Grid container>
          <Grid  item xs={12} md={12} sm={12} lg={2} >
            <ProductFilter  />
          </Grid>
          <Grid item xs={12} md={12} sm={12} lg={10}>
            <Grid  container textAlign="center" p={3}>
              {products.map((product, index) => (
                <ProductCard product={product} key={index} />
              ))}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={12} sm={12} lg={12} >
          <Grid container justifyContent={{ xs: "center", sm: "flex-end" }} >
            <Pagination
              count={noPage}
              defaultPage={1}
              onChange={onChangePagination}
              component="div"
            />
          </Grid>

        </Grid>

      </Grid>
    </>
  )
}

export default ProductList