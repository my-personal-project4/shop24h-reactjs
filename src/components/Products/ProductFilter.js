import { Grid,TextField, Typography, Button, Select, MenuItem, FormControl,} from "@mui/material";
import { useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { nameChange, minPriceChange, maxPriceChange,brandChange, colorChange, sizeChange} from "../../Redux/Actions/FilterValueAction";
import { getAllProductFilter } from "../../Redux/Actions/GetAllProductAction";
import { getType } from "../../Redux/Actions/getTypeAction";


function ProductFilter() {
  const dispatch = useDispatch();
  const {productName, minPrice, maxPrice,color,size,type} = useSelector(reduxData=> reduxData.filterValueReducer);
  const {productType} = useSelector(reduxData => reduxData.getTypeReducer);
  const filterProduct = () => {
    dispatch(getAllProductFilter(productName,minPrice, maxPrice,type,color,size));
  }

  useEffect(()=> {
    dispatch(getType());
  },[]);

  return(
    <>
     <Grid textAlign="center" item xs={12} lg={12} md={12} sm={12} mt={5} padding={1}>
        <Typography variant="h6" gutterBottom component="h5">
                    Name
        </Typography>
        <TextField sx={{ m: 1, minWidth: 50 }} value={productName} onChange={(event)=> dispatch(nameChange(event.target.value))} label="Name *" variant="outlined" fullWidth />
    </Grid>
     <Grid textAlign="center"  item xs={12} lg={12} md={12} sm={12} mt={5} padding={1}>
        <Typography variant="h6" gutterBottom component="h5">
                        Color
        </Typography>
        <FormControl fullWidth sx={{ m: 1, minWidth: 50 }}  >
          <Select
            value={color}
            onChange={(e) => dispatch(colorChange(e.target.value))}
            
          >
            <MenuItem value="ALL">All Color</MenuItem>
            <MenuItem value="Black">Black</MenuItem>
            <MenuItem value="Blue">Blue</MenuItem>
            <MenuItem value="Green">Green</MenuItem>
            <MenuItem value="Purple">Purple</MenuItem>
            <MenuItem value="Pink">Pink</MenuItem>
            <MenuItem value="Red">Red</MenuItem>
            <MenuItem value="Yellow">Yellow</MenuItem>
          </Select>
      </FormControl>
    </Grid>
     <Grid textAlign="center"  item xs={12} lg={12} md={12} sm={12} mt={5} padding={1}>
        <Typography variant="h6" gutterBottom component="h5">
                        Size
        </Typography>
        <FormControl fullWidth sx={{ m: 1, minWidth: 50 }}  >
          <Select
            value={size}
            onChange={(event) => dispatch(sizeChange(event.target.value))}
            
          >
            <MenuItem value="ALL">All Size</MenuItem>
            <MenuItem value="35">35</MenuItem>
            <MenuItem value="36">36</MenuItem>
            <MenuItem value="37">37</MenuItem>
            <MenuItem value="38">38</MenuItem>
            <MenuItem value="39">39</MenuItem>
            <MenuItem value="40">40</MenuItem>
            <MenuItem value="41">41</MenuItem>
            <MenuItem value="42">42</MenuItem>
          </Select>
      </FormControl>
    </Grid>
     <Grid textAlign="center"  item xs={12} lg={12} md={12} sm={12} mt={5} padding={1}>
        <Typography variant="h6"   gutterBottom component="h5">
                    Category
        </Typography>
        <FormControl fullWidth sx={{ m: 1, minWidth: 50 }}  >
          <Select
            value={type}
            onChange={(event) => dispatch(brandChange(event.target.value))}
            
          >
            <MenuItem value="ALL">All Category</MenuItem>
            {productType.map((item,index) => {
              return (
                <MenuItem key={index} value={item._id}>{item.name}</MenuItem>
              )
            })}
          </Select>
      </FormControl>
    </Grid>
    <Grid  textAlign="center" item xs={12} lg={12} md={12} sm={12} mt={5} padding={1}>
        <Typography variant="h6" gutterBottom component="h5">
                    Price
        </Typography>
        <Grid container item xs={12} lg={12} md={12} sm={12} mt={3}>
                <Grid item xs={12} lg={12} md={12} sm={12} padding={1}>
                    <TextField sx={{ m: 1, minWidth: 50 }} type="number" value={minPrice} onChange={(event)=> dispatch(minPriceChange(event.target.value))} label="MinPrice *" variant="outlined" fullWidth />
                </Grid>
                <Grid item xs={12} lg={12} md={12} sm={12} padding={1}>
                    <TextField sx={{ m: 1, minWidth: 50 }} type="number" value={maxPrice} onChange={(event)=> dispatch(maxPriceChange(event.target.value))}  label="MaxPrice *" variant="outlined" fullWidth />
                </Grid>
        </Grid>
        <Grid item xs={12} lg={12} md={12} sm={12} mt={3} mb={5} padding={1}>
            <Button sx={{ m: 1, minWidth: 50 }} onClick={filterProduct} color="success" variant="contained" size="large" fullWidth>Filter</Button>
        </Grid>    
    </Grid>
    
    </>
  )
}

export default ProductFilter