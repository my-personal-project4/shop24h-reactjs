import React, { useEffect} from "react";
import {
  Grid,
  Card,
  CardContent,
  Typography,
  CardActionArea,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getProductsLimit } from "../../Redux/Actions/GetAllProductAction";

 function Product1s (props) {
  const dispatch = useDispatch();
  const { products} = useSelector((reduxData) => reduxData.getProductsReducer);
  const navigate = useNavigate();
  const onCardClick = (id) => {
    navigate(`/products/${id}`);
  };
  useEffect(() => {
    dispatch(getProductsLimit(8));
  }, []);


  const displayProduct = () => {
    return (
      <>
        <Grid container textAlign="center" p={3}>
          {products.map((product,index) => {
            return(
              <Grid  key={index} item xs={12} md={3} sm={6} lg={3} py={3} style={{ display: "flex", textAlign: "center", justifyContent: "center" }} >
                <Card sx={{ maxWidth: 345, height: "100%" ,  }}>
                  <CardActionArea onClick={()=>onCardClick(product._id)} >
                      <CardContent>
                        <img
                          src={product.imageUrl}
                          alt={product.name}
                          style={{ maxWidth: "100%", width: "200px", height: "200px", objectFit: "fill", marginRight: 10 }}
                        />
                        <Typography gutterBottom variant="h6" component="div">
                          {product.name}
                        </Typography>
                        <Typography variant="h5" color="red">
                          {product.buyPrice} $
                        </Typography>
                       
                      </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )
          })}
        </Grid>
      </>
    )
  };
  return (
    <div className="container">
      <h2 style={{marginTop: 30}}>LATEST PRODUCT</h2>
      {displayProduct()}
    </div>
  );
}

export default Product1s