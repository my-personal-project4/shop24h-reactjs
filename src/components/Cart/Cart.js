import {
  Button,
  Grid,
  IconButton,
  TableContainer,
  TableBody,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  Avatar,
  Typography,
  Container,
  Snackbar,
  Alert,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import DeleteIcon from "@mui/icons-material/Delete";
import { useDispatch } from "react-redux";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { useNavigate } from "react-router-dom";
import {
  decreaseItem1,
  deleteCartItem,
  increaseItem1,
} from "../../Redux/Actions/CartAction";
import BreadCrumb from "../BreadCrumb/BreadCrumb";
import { styled } from "@mui/material/styles";
import SimilarProducts1 from "./SimilarProducts1";
import { purple } from "@mui/material/colors";

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: purple[500],
  "&:hover": {
    backgroundColor: purple[700],
  },
}));

function Cart() {
  const breadcrumbArray = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Shop",
      url: "/products",
    },
  ];
  const navigate = useNavigate();
  const { cartItem, itemQuantity } = useSelector(
    (reduxData) => reduxData.cardReducer
  );
  const dispatch = useDispatch();
  const [isOpenAlert, setIsOpenAlert] = useState(false);
  const [statusModal, setStatusModal] = useState("error");
  const [noidungAlertValid, setNoidungAlertValid] = useState("");

  const addition = (acc, currentValue) => {
    return acc + currentValue.buyPrice * currentValue.quantity;
  };
  const addition1 = (acc, currentValue) => {
    return acc + currentValue.quantity;
  };

  const totalPrice = cartItem.reduce(addition, 0);
  const totalItem = cartItem.reduce(addition1, 0);

  const onDeleteProductHandler = (productInfo, productIndex) => {
    dispatch(deleteCartItem(productInfo, productIndex));
  };

  const increaseItem = (product) => {
    dispatch(increaseItem1(product, 1));
  };
  const decreaseItem = (item, itemIndex) => {
    dispatch(decreaseItem1(item, itemIndex));
  };
  const handleCloseAlert = () => {
    setIsOpenAlert(false);
  };
  function formatCash(cash) {
    if (cash) return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
  useEffect(() => {
    localStorage.setItem("itemQuantity", JSON.stringify(itemQuantity));
    localStorage.setItem("cartItem", JSON.stringify(cartItem));
  }, [cartItem, itemQuantity]);

  const onBuyClick = () => {
    if (cartItem.length === 0) {
      setIsOpenAlert(true);
      setStatusModal("error");
      setNoidungAlertValid("Please shopping something!!!");
    } else {
      navigate(`/CheckOut`);
    }
  };
  const onShoppingClick = () => {
    navigate("/products");
  };
  return (
    <>
      <Container>
        <BreadCrumb
          breadcrumbArray={breadcrumbArray}
          pageName="Shopping Cart"
        />
      </Container>

      <Grid container mt={2}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead style={{ backgroundColor: "#f1f3f4" }}>
              <TableRow>
                <TableCell align="left">Product</TableCell>
                <TableCell align="left">Price</TableCell>
                <TableCell align="left">Quantity</TableCell>
                <TableCell align="left">Total</TableCell>
                <TableCell align="left">Remove Product</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {cartItem.length > 0 ? (
                cartItem.map((product, index) => {
                  return (
                    <TableRow key={index}>
                      <TableCell>
                        <Grid container>
                          <Avatar
                            src={product.imageUrl}
                            alt={`img ${index}`}
                            variant="square"
                            sx={{ width: 56, height: 56, marginRight: "10px" }}
                          />
                          {product.name}
                        </Grid>
                      </TableCell>
                      <TableCell>{product.buyPrice} $</TableCell>
                      <TableCell>
                        <IconButton
                          onClick={() => {
                            decreaseItem(product, index);
                          }}
                          color="primary"
                          aria-label="remove a product"
                          style={{ height: "40px", width: "40px" }}
                        >
                          <RemoveIcon />
                        </IconButton>
                        {product.quantity}
                        <IconButton
                          onClick={() => {
                            increaseItem(product);
                          }}
                          color="primary"
                          aria-label="add a product"
                          style={{ height: "40px", width: "40px" }}
                        >
                          <AddIcon />
                        </IconButton>
                      </TableCell>

                      <TableCell>
                        {formatCash(product.buyPrice * product.quantity)  } $
                      </TableCell>
                      <TableCell>
                        <IconButton
                          onClick={() => {
                            onDeleteProductHandler(product, index);
                          }}
                        >
                          <DeleteIcon />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })
              ) : (
                <TableRow style={{ margin: "auto" }}>
                  <TableCell>
                    <p>Your shopping cart is empty</p>
                    <br></br>
                    <Button
                      style={{ borderRadius: "30px" }}
                      variant="outlined"
                      className="cart"
                      onClick={onShoppingClick}
                    >
                      Go Shopping Now
                    </Button>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>

      <Grid container mt={1}>
        <Grid item xs={12} textAlign="center">
          <Typography
            variant="h5"
            sx={{
              display: "inline",
              marginRight: "15px",
              color: "orange",
              fontWeight: "bold",
            }}
          >
            Total ({totalItem} item): {formatCash(totalPrice)} $
          </Typography>
          <ColorButton
            variant="contained"
            style={{ borderRadius: "30px" }}
            size="small"
            onClick={onBuyClick}
          >
            PROCEED TO CHECKOUT
          </ColorButton>
        </Grid>
      </Grid>
      <Grid style={{ marginTop: "100px" }}>
        <Grid item xs={12}>
          <SimilarProducts1 />
        </Grid>
      </Grid>
      <Snackbar
        open={isOpenAlert}
        autoHideDuration={3000}
        onClose={handleCloseAlert}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseAlert}
          severity={statusModal}
          sx={{ width: "100%" }}
        >
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </>
  );
}
export default Cart;
