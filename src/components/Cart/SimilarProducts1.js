import { Grid, Typography } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProductsSimilar } from "../../Redux/Actions/GetAllProductAction";
// import Swiper core and required modules
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import ProductCardSimilar1 from "./ProductCardSimilar1";

function SimilarProducts1() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const dispatch = useDispatch();
  const { products } = useSelector((reduxData) => reduxData.getProductsReducer);

  useEffect(() => {
    dispatch(getProductsSimilar());
  }, []);
  return (
    <Grid container textAlign="center" spacing={2} mt={2}>
      <Grid item xs={12} md={12} sm={12} lg={12}>
        <Typography
          variant="h5"
          ml={2}
          fontWeight="bold"
          textAlign="start"
          style={{ color: "orange" }}
        >
          YOU MAY ALSO LIKE
        </Typography>
      </Grid>
      <Swiper
        // install Swiper modules
        modules={[Navigation, Pagination, Scrollbar, A11y]}
        loop={true}
        spaceBetween={10}
        slidesPerView={4}
        // navigation
        // pagination={{ clickable: true }}
        // scrollbar={{ draggable: true }}
        breakpoints={{
          0: {
            slidesPerView: 1,
            spaceBetween: 10,
          },
          480: {
            slidesPerView: 2,
            spaceBetween: 10,
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 10,
          },
          1024: {
            slidesPerView: 4,
            spaceBetween: 10,
          },
        }}
      >
        {products.map((product, index) => {
          return (
            <SwiperSlide key={index}>
              <ProductCardSimilar1 product={product} />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </Grid>
  );
}
export default SimilarProducts1;
