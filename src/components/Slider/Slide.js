import Carousel from "react-material-ui-carousel";
import "./style.css";
// import { LinkBtn } from "../btn/LinkBtn";
import slide1 from '../../assets/images/slide-04.jpg';
import slide2 from '../../assets/images/slide-05.jpg';
import slide3 from '../../assets/images/slide-07.jpg';

export function Slide() {
  let data = [
    {
      id: 1,
      mainTitle: "Women Collection 2021",
      subTitle: "NEW SEASON",
      img: slide1,
    },
    {
      id: 2,
      mainTitle: "Men Collection 2021",
      subTitle: "JACKETS & COATS",
      img: slide2,
    },
    {
      id: 3,
      mainTitle: "Men Collection 2021",
      subTitle: "NEW ARRIVALS",
      img: slide3,
    },
  ];
  const Slider = () => (
    <Carousel
      className="postion"
      interval="3000"
      navButtonsProps={{
        style: {
          display: "none",
        },
      }}
      indicatorIconButtonProps={{
        style: {
          display: "none",
        },
      }}
    >
      {data.map((e) => (
        <div key={e.id}>
          <img className=" vh100" src={e.img} alt="First slide" />
          {/* <div className="title">
            <div>{e.mainTitle}</div>
            <div>{e.subTitle}</div>

            <LinkBtn to="/Shop"> Shope Now</LinkBtn>
          </div> */}
        </div>
      ))}
    </Carousel>
  );

  return <>{Slider()}</>;
}
