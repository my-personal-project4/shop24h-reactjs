import { useState, useEffect } from "react";
import * as React from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { Header, Logo, Bar, ShopIcons } from "./styled";
import { useSelector, useDispatch } from "react-redux";
import { themeAction } from "../../Redux/Actions/Action";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import { deepOrange, deepPurple } from "@mui/material/colors";

import auth from "../../firebase";
import { signOut, onAuthStateChanged } from "firebase/auth";
import {
  emailLoginAction,
  userLoginAction,
} from "../../Redux/Actions/UserLoginAction";
import { MenuItem, Menu, Tooltip, Button } from "@mui/material";
import { CLEAR_ALL_ORDERS } from "../../Redux/Constants/getOrdersConstant";
import { clearStorage } from "../../Redux/Actions/getOrdersAction";
import Badge from "@mui/material/Badge";
import { styled } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";

// import SettingsIcon from '@mui/icons-material/Settings';
// const settings = [ 'Login','Logout','Profile'];
const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    right: -3,
    top: 1,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: "0 4px",
  },
}));
export const NavBar = (props) => {
  const [ToggleNav, setToggleNav] = useState(false);
  const [CheckScroll, setCheckScroll] = useState(false);
  const themetoggle = useSelector((state) => state.theme);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  // const dispatchtheme = useDispatch();
  const dispatch = useDispatch();
  const { user, emailLogin } = useSelector(
    (reduxData) => reduxData.userLoginReducer
  );
  const { cartItem } = useSelector((reduxData) => reduxData.cardReducer);
  const changeColor = () => {
    if (window.scrollY >= 60) {
      setCheckScroll(true);
    } else {
      setCheckScroll(false);
    }
  };
  window.addEventListener("scroll", changeColor);
  const navigate = useNavigate();
  const logOut = () => {
    signOut(auth)
      .then((result) => {
        // console.log(result);
        dispatch(userLoginAction(null));
        dispatch(emailLoginAction(null));
        dispatch(clearStorage());
        navigate("/");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      if (result) {
        dispatch(userLoginAction(result));
        dispatch(emailLoginAction(result));
      } else {
        dispatch(userLoginAction(null));
        dispatch(emailLoginAction(null));
      }
    });
  }, [user]);
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  return (
    <>
      <Header className={CheckScroll ? "background" : ""}>
        <div className="container">
          <Logo>
            <Link to="/">
              <img
                src={`${
                  themetoggle
                    ? "https://omarabualhija.github.io/CozaStore/images/icons/logo-02.png"
                    : "https://omarabualhija.github.io/CozaStore/images/icons/logo-01.png"
                }`}
                alt="logo"
                width="110%"
              />
            </Link>
          </Logo>

          <Bar className={!ToggleNav ? "toggle" : ""}>
            <NavLink onClick={() => setToggleNav(!ToggleNav)} to="/">
              <strong style={{ fontSize: "20px" }}>Home</strong>
            </NavLink>
            <NavLink onClick={() => setToggleNav(!ToggleNav)} to="/products">
              <strong style={{ fontSize: "20px" }}>Shop</strong>
            </NavLink>
            <NavLink onClick={() => setToggleNav(!ToggleNav)} to="/About">
              <strong style={{ fontSize: "20px" }}>About</strong>
            </NavLink>

            <NavLink onClick={() => setToggleNav(!ToggleNav)} to="/Contact">
              <strong style={{ fontSize: "20px" }}>Contact</strong>
            </NavLink>
            <NavLink onClick={() => setToggleNav(!ToggleNav)}>
              <Tooltip title="Open more">
                <Button onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  {user || emailLogin ? (
                    <Stack direction="row" spacing={2}>
                      <b>{user.displayName || emailLogin.email}</b>
                      <Avatar
                        sx={{
                          bgcolor: deepOrange[500],
                          width: 24,
                          height: 24,
                        }}
                      >
                        N
                      </Avatar>
                    </Stack>
                  ) : (
                    <b style={{ fontSize: "20px" }}>My Account</b>
                  )}
                </Button>
              </Tooltip>
              <Menu
                sx={{ mt: "45px" }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                <MenuItem>
                  <NavLink to="/login">Login</NavLink>
                </MenuItem>
                <MenuItem onClick={logOut}>Logout</MenuItem>
                <MenuItem>
                  {user || emailLogin ? (
                    <NavLink to="/myOrders"> My Orders</NavLink>
                  ) : (
                    <NavLink to="/login">My Orders</NavLink>
                  )}
                </MenuItem>
              </Menu>
            </NavLink>
          </Bar>

          <ShopIcons>
            {/* <div
              onClick={() => dispatch(themeAction())}
              className={themetoggle ? `fas fa-sun` : `far fa-moon`}
            ></div> */}
            <Link to="/Cart">
              <IconButton aria-label="cart">
                <StyledBadge
                  showZero
                  badgeContent={cartItem.length}
                  color="secondary"
                >
                  <ShoppingCartIcon sx={{ color: "orange" }} />
                </StyledBadge>
              </IconButton>
              {/* Cart({cartItem.length}) */}
            </Link>
            {/* <div className="far fa-heart"></div> */}
            <div
              onClick={() => setToggleNav(!ToggleNav)}
              className={!ToggleNav ? "fas fa-bars" : "fas fa-times fa-xl"}
            ></div>
          </ShopIcons>
        </div>
      </Header>
    </>
  );
};
