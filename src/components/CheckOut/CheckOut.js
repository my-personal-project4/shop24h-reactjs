import { Grid, Typography, Alert, Snackbar, Button } from "@mui/material";
import { Container } from "@mui/system";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import BreadCrumb from "../BreadCrumb/BreadCrumb";
import DeliveryInfo from "./DeliveryInfo";
import OrderProduct from "./OrderProduct";
import swal from "sweetalert";
import { createOrder } from "../../Redux/Actions/CheckOutAction";
import { clearStorage } from "../../Redux/Actions/CartAction";

function CheckOut() {
  const breadcrumbArray = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Shop",
      url: "/products",
    },
    {
      name: "Cart",
      url: "/Cart",
    },
  ];
  const { cartItem, itemQuantity } = useSelector(
    (reduxData) => reduxData.cardReducer
  );
  const addition = (acc, currentValue) => {
    return acc + currentValue.buyPrice * currentValue.quantity;
  };
  const totalPrice = cartItem.reduce(addition, 0);
  const { fullname, email, address, phoneNumber, note, country, city } =
    useSelector((reduxData) => reduxData.checkOutReducer);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isOpenAlert, setIsOpenAlert] = useState(false);
  const [alertContent, setAlertContent] = useState("");
  const [alertStatus, setAlertStatus] = useState("warning");
  function formatCash(cash) {
    if (cash) return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  useEffect(() => {
    localStorage.setItem("itemQuantity", JSON.stringify(itemQuantity));
    localStorage.setItem("cartItem", JSON.stringify(cartItem));
  }, [cartItem, itemQuantity]);

  const onBtnConfirmClick = () => {
    let vDataForm = {
      fullName: fullname,
      email: email,
      address: address,
      phoneNumber: phoneNumber,
      note: note,
      country: country,
      city: city,
    };
    var isValidate = validateData(vDataForm);
    if (isValidate) {
      dispatch(createOrder(vDataForm, cartItem, totalPrice));
      dispatch(clearStorage());
      swal("Đơn hàng đã đặt thành công", "", "success");
    }
  };
  const validateData = (paramData) => {
    if (paramData.fullname === "") {
      setIsOpenAlert(true);
      setAlertContent("Full name is invalid");
      setAlertStatus("warning");
      return false;
    }
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!emailRegex.test(paramData.email)) {
      setIsOpenAlert(true);
      setAlertContent("Email is invalid");
      setAlertStatus("warning");
      return false;
    }
    if (paramData.address === "") {
      setIsOpenAlert(true);
      setAlertContent("Address is invalid");
      setAlertStatus("warning");
      return false;
    }
    if (paramData.phoneNumber === "") {
      setIsOpenAlert(true);
      setAlertContent("Phone Number is invalid");
      setAlertStatus("warning");
      return false;
    }
    return true;
  };
  const handleCloseAlert = () => {
    setIsOpenAlert(false);
  };

  return (
    <>
      <Container>
        <BreadCrumb breadcrumbArray={breadcrumbArray} pageName="Check Out" />
        <Grid container>
          <Grid item xs={12} lg={6} md={12} sm={12} paddingRight={5}>
            <DeliveryInfo />
          </Grid>
          <Grid item xs={12} lg={6} md={12} sm={12}>
            <Grid item xs={12} lg={12} md={12} sm={12} paddingBottom={3}>
              <OrderProduct />
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} >
              <Grid
                container
                mt={4}
                spacing={2}
                p={3}
                sx={{
                  border: "double ",
                  borderRadius: "10px",
                  backgroundColor: "#fff",
                }}
              >
                <Grid item xs={12} sm={12} lg={6} md={12}>
                  <Typography>
                    Total Price:{" "}
                    <span
                      style={{
                        color: "orange",
                        fontWeight: "bold",
                        fontSize: "x-large",
                      }}
                    >
                      {formatCash(totalPrice)} $
                    </span>
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={12} lg={6} md={12} textAlign="end">
                  <Button
                    href="/Cart"
                    sx={{ marginRight: "5px" }}
                    variant="contained"
                    color="primary"
                    size="large"
                  >
                    Cancel
                  </Button>
                  <Button
                    variant="contained"
                    color="warning"
                    size="large"
                    onClick={onBtnConfirmClick}
                  >
                    Confirm
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <Snackbar
        open={isOpenAlert}
        autoHideDuration={6000}
        onClose={handleCloseAlert}
      >
        <Alert
          onClose={handleCloseAlert}
          severity={alertStatus}
          sx={{ width: "100%" }}
        >
          {alertContent}
        </Alert>
      </Snackbar>
    </>
  );
}

export default CheckOut;
