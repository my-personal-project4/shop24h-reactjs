import { Grid, MenuItem, Select, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import Autocomplete from "@mui/material/Autocomplete";
import {
  fullnameChange,
  emailChange,
  phoneChange,
  cityChange,
  countryChange,
  messageChange,
  addressChange,
  getAllCity,
} from "../../Redux/Actions/CheckOutAction";
import auth from "../../firebase";
function DeliveryInfo() {
  const dispatch = useDispatch();
  const {
    fullname,
    email,
    address,
    phoneNumber,
    note,
    country,
    city,
    allCity,
  } = useSelector((reduxData) => reduxData.checkOutReducer);
  const [value, setValue] = useState("");
  const [inputValue, setInputValue] = useState("");
  // console.log(value);
  // console.log(inputValue);
  useEffect(() => {
    auth.onAuthStateChanged((result) => {
      dispatch({
        type: "GET_USER_INFOMATION",
        value: result,
      });
    });
  }, []);
  let cityFake = allCity.map((country, index) => {
    return {
      label: country.country,
      id: index,
      cities: country.cities,
    };
  });
  let districtFake =
    value !== "" || inputValue !== "" 
      ? cityFake.filter((item) => item.label.toLowerCase().includes(inputValue.toLowerCase()))
      : null;
  let districtCity1 = districtFake
    ? districtFake[0].cities.map((districtNew) => {
        return {
          label: districtNew,
        };
      })
    : null;
  useEffect(() => {
    dispatch(getAllCity());
  }, [inputValue]);

  const changeFullnameHandler = (event) => {
    dispatch(fullnameChange(event.target.value));
  };
  const changeAddressHandler = (event) => {
    dispatch(addressChange(event.target.value));
  };
  const changeEmailHandler = (event) => {
    dispatch(emailChange(event.target.value));
  };
  const changeCityHandler = (event) => {
    dispatch(cityChange(event.target.value));
  };
  const changeCountryHandler = (event) => {
    dispatch(countryChange(event.target.value));
  };
  const changeNoteHandler = (event) => {
    dispatch(messageChange(event.target.value));
  };
  const changePhoneNumberHandler = (event) => {
    dispatch(phoneChange(event.target.value));
  };
  return (
    <div>
      <Grid
        container
        mt={1}
        spacing={2}
        p={3}
        sx={{ border: "double", borderRadius: "10px", backgroundColor: "#fff" }}
      >
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <Typography variant="h5">Address Orders</Typography>
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <TextField
            size="small"
            fullWidth
            label="Full name"
            value={fullname}
            id="fullname"
            onChange={changeFullnameHandler}
          />
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <TextField
            size="small"
            fullWidth
            label="Phone Number"
            value={phoneNumber}
            onChange={changePhoneNumberHandler}
          />
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <TextField
            size="small"
            fullWidth
            label="Email"
            value={email}
            onChange={changeEmailHandler}
          />
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <Autocomplete
            disablePortal
            id="combo-box-demo"
            options={cityFake}
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
            }}
            inputValue={inputValue}
            onInputChange={(event, newInputValue) => {
              setInputValue(newInputValue);
              dispatch(countryChange(newInputValue));
            }}
            sx={{ width: "100%" }}
            renderInput={(params) => <TextField {...params} label="country" />}
          />
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <Autocomplete
            disablePortal
            id="combo-box-demo"
            options={districtCity1}
            onInputChange={(event, newInputValue) => {
              dispatch(cityChange(newInputValue));
            }}
            sx={{ width: "100%" }}
            renderInput={(params) => <TextField {...params} label="city" />}
          />
        </Grid>
        {/* <Grid item xs={12} sm={12} lg={12} md={12}>
          <Select fullWidth value={country} onChange={changeCountryHandler}>
            <MenuItem value="ALL">Choose City</MenuItem>
            {allCity.map((countryItem, index) => {
              return (
                <MenuItem key={index} value={countryItem.country}>
                  {countryItem.country}
                </MenuItem>
              );
            })}
          </Select>
        </Grid> */}
        {/* <Grid item xs={12} sm={12} lg={12} md={12}>
          <Select fullWidth value={city} onChange={changeCityHandler}>
            <MenuItem value="ALL">Choose District</MenuItem>
            {allCity.length > 0 && country !== "ALL"
              ? allCity
                  .filter((item) => {
                    return item.country
                      .toLowerCase()
                      .includes(country.toLowerCase());
                  })
                  .map((cityItem) =>
                    cityItem.cities.map((districtItem, index) => {
                      return (
                        <MenuItem key={index} value={districtItem}>
                          {districtItem}
                        </MenuItem>
                      );
                    })
                  )
              : null}
          </Select>
        </Grid> */}

        <Grid item xs={12} sm={12} lg={12} md={12}>
          <TextField
            size="small"
            fullWidth
            label="Address"
            value={address}
            onChange={changeAddressHandler}
          />
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <TextField
            size="small"
            fullWidth
            label="Note"
            value={note}
            onChange={changeNoteHandler}
          />
        </Grid>
      </Grid>
    </div>
  );
}

export default DeliveryInfo;
