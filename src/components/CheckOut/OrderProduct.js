import {
  Grid,
  Typography,
  Avatar,
  TableCell,
  TableHead,
  Paper,
  Table,
  TableRow,
  TableBody,
  TableContainer,
} from "@mui/material";
import { useSelector } from "react-redux";
function OrderProduct() {
  const { cartItem } = useSelector((reduxData) => reduxData.cardReducer);
  function formatCash(cash) {
    if (cash) return cash.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
  // const addition = (acc, currentValue) => {
  //     return acc + currentValue.buyPrice * currentValue.quantity;
  //   };
  // const totalPrice = cartItem.reduce(addition, 0);
  return (
    <Grid
      container
      mt={1}
      spacing={2}
      p={3}
      sx={{ border: "double", borderRadius: "10px", backgroundColor: "#fff" }}
    >
      <Grid item xs={12} sm={12} lg={12} md={12}>
        <Typography variant="h5">Products</Typography>
      </Grid>
      <Grid item xs={12} sm={12} lg={12} md={12}>
        <TableContainer component={Paper}>
          <Table  aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Products</TableCell>
                <TableCell align="left">Price</TableCell>
                <TableCell align="left">Quantity</TableCell>
                <TableCell align="left">Total</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {cartItem.map((product, index) => {
                return (
                  <TableRow key={index}>
                    <TableCell>
                      <Grid container>
                        <Avatar
                          src={product.imageUrl}
                          alt={`img ${index}`}
                          variant="square"
                          sx={{ width: 56, height: 56, marginRight: "10px" }}
                        />
                        {product.name}
                      </Grid>
                    </TableCell>
                    <TableCell>{product.buyPrice} $</TableCell>
                    <TableCell>{product.quantity}</TableCell>
                    <TableCell>
                      {formatCash(product.buyPrice * product.quantity)} $
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default OrderProduct;
