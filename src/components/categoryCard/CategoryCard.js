import React from "react";
import { Card } from "./styled.js";
import { Link } from "react-router-dom";
import banner1 from '../../assets/images/Air Jordan 200.webp';
import banner2 from '../../assets/images/Nike Go FlyEase.webp';
import banner3 from '../../assets/images/NMD_R1.webp';
import banner4 from '../../assets/images/Voyage Nitro.jpg';

export function CategoryCard() {
  return (
    <Card>
      <div className="container">
        <Link to="/products">
          <img
            src={banner1}
            alt="banner-01"
          ></img>
          <div>
            <h4>Jordan</h4>
          </div>
          <div>Shope Now</div>
        </Link>

        <Link to="/products">
          <img
            src={banner2}
            alt="banner-02"
          ></img>
          <div>
            {/*title*/}
            <h4>Nike</h4>
          </div>
          <div>Shope Now</div>
        </Link>
        <Link to="/products">
          <img
            src={banner3}
            alt="banner-03"
          ></img>
          <div>
            <h4>Adidas</h4>
          </div>
          <div>Shope Now</div>
        </Link>
        <Link to="/products">
          <img
            src={banner4}
            alt="banner-04"
          ></img>
          <div>
            <h4>Puma</h4>
          </div>
          <div>Shope Now</div>
        </Link>
      </div>
    </Card>
  );
}
