import { styled } from "@mui/material/styles";
import { Grid, Rating, Typography } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { getReviews } from "../../Redux/Actions/reviewAction";
import { Stack } from "@mui/system";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import { deepOrange, deepPurple } from "@mui/material/colors";
import Avatar from "@mui/material/Avatar";

const StyledRating = styled(Rating)({
  "& .MuiRating-iconFilled": {
    color: "#ff6d75",
  },
  "& .MuiRating-iconHover": {
    color: "#ff3d47",
  },
});
function Reviews() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { productid } = useParams();
  const { reviews } = useSelector((redux) => redux.getReviewsReducer);
  function makeid(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  useEffect(() => {
    dispatch(getReviews(productid));
  }, [productid]);
  return (
    <>
      <div
        style={{
          width: "100%",
          height: "3px",
          backgroundColor: "green",
          marginTop: "80px",
        }}
      ></div>
      <Grid mt={2} container>
        <Grid item xs={12} md={12} sm={12} lg={12}>
          <Typography
            variant="h5"
            fontWeight="bold"
            textAlign="center"
            style={{ color: "#8f5741" }}
          >
            Reviews
          </Typography>
        </Grid>
        {reviews.length > 0 ? (
          reviews.map((review, index) => {
            return (
              <Grid container key={index} style={{ width: "100%"}}>
                <Stack direction="row" spacing={2}>
                  <Avatar sx={{ bgcolor: deepOrange[500] }}>{makeid(1)}</Avatar>
                </Stack>
                <Grid container>
                  <Grid item xs={12}>
                    <strong style={{ fontWeight: 700, marginLeft:10 }}>
                      {review.fullName}
                    </strong>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12}>
                    <strong style={{ fontWeight: 300, marginLeft:10 }}>{review.email}</strong>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12}>
                    <Stack spacing={1}>
                      <StyledRating  style={{ fontWeight: 300, marginLeft:10 }}
                        icon={<FavoriteIcon fontSize="inherit" />}
                        emptyIcon={<FavoriteBorderIcon fontSize="inherit" />}
                        value={review.rating}
                        readOnly
                      />
                    </Stack>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12}>
                    <label style={{ marginLeft:10 }}>{review.description}</label>
                  </Grid>
                </Grid>
                <div
                  style={{
                    width: "100%",
                    height: "3px",
                    backgroundColor: "beige",
                    marginBottom: "30px",
                  }}
                ></div>
              </Grid>
            );
          })
        ) : (
          <p> No Reviews </p>
        )}
      </Grid>
    </>
  );
}

export default Reviews;
