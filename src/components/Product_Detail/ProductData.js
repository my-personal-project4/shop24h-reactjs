import {
  Button,
  Grid,
  IconButton,
  Stack,
  Typography,
  Alert,
  Snackbar,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { getProduct } from "../../Redux/Actions/GetAllProductAction";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Thumbs } from "swiper";
import "./product-image-slider.scss";
import { addCartItem } from "../../Redux/Actions/CartAction";
import { useNavigate } from "react-router-dom";

function ProductData() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { productid } = useParams();
  const { productInfo } = useSelector(
    (reduxData) => reduxData.getProductsReducer
  );
  const { cartItem, itemQuantity } = useSelector(
    (reduxData) => reduxData.cardReducer
  );
  const [quantity, setQuantity] = useState(1);
  const [isOpenAlert, setIsOpenAlert] = useState(false);
  const [statusModal, setStatusModal] = useState("error");
  const [noidungAlertValid, setNoidungAlertValid] = useState("");
  const { user, emailLogin } = useSelector(
    (reduxData) => reduxData.userLoginReducer
  );
  const [color, setColor] = useState("ALL");
  const [size, setSize] = useState("ALL");
  useEffect(() => {
    dispatch(getProduct(productid));
  }, [productid]);

  // console.log(size);
  const [activeThumb, setActiveThumb] = useState();

  const addQuantity = () => {
    setQuantity(quantity + 1);
  };
  const reduceQuantity = () => {
    if (quantity > 1) setQuantity(quantity - 1);
  };
  const addToCard = (productInfo) => {
    if (validate()) {
      if (user === null || emailLogin === null) {
        navigate("/login");
      } else {
        dispatch(addCartItem(productInfo, quantity));
        setIsOpenAlert(true);
        setStatusModal("success");
        setNoidungAlertValid("Product added to your cart");
      }
    }
  };
  const validate = () => {
    if (color === "ALL") {
      setIsOpenAlert(true);
      setStatusModal("error");
      setNoidungAlertValid("Please select color");
      return false;
    }
    if (size === "ALL") {
      setIsOpenAlert(true);
      setStatusModal("error");
      setNoidungAlertValid("Please select size");
      return false;
    }
    return true;
  };
  useEffect(() => {
    localStorage.setItem("itemQuantity", JSON.stringify(itemQuantity));
    localStorage.setItem("cartItem", JSON.stringify(cartItem));
  });
  const handleCloseAlert = () => {
    setIsOpenAlert(false);
  };
  return (
    <Grid container mt={3} spacing={{ xs: 1, sm: 2, md: 5 }}>
      <Grid item xs={12} md={6} sm={6} lg={6}>
        <Swiper
          loop={true}
          spaceBetween={10}
          navigation={true}
          modules={[Navigation, Thumbs]}
          grabCursor={true}
          thumbs={{ swiper: activeThumb }}
          className="product-images-slider"
        >
          {productInfo !== ""
            ? productInfo.imageChild.map((item, index) => (
                <SwiperSlide key={index}>
                  <img src={item} alt="product images" />
                </SwiperSlide>
              ))
            : null}
        </Swiper>
        <Swiper
          onSwiper={setActiveThumb}
          loop={true}
          spaceBetween={10}
          slidesPerView={4}
          modules={[Navigation, Thumbs]}
          className="product-images-slider-thumbs"
        >
          {productInfo !== ""
            ? productInfo.imageChild.map((item, index) => (
                <SwiperSlide key={index}>
                  <div className="product-images-slider-thumbs-wrapper">
                    <img src={item} alt="product images" />
                  </div>
                </SwiperSlide>
              ))
            : null}
        </Swiper>
      </Grid>
      <Grid item xs={12} md={6} sm={6} lg={6} my="auto">
        <Grid container spacing={1} textAlign="center">
          <Grid item xs={12} md={12} sm={12} lg={12}>
            <Typography variant="h4">{productInfo.name}</Typography>
          </Grid>
          <Grid item xs={12} md={12} sm={12} lg={12}>
            <Typography variant="h5">
              Brand: {productInfo ? productInfo.type.name : null}
            </Typography>
          </Grid>
          <Grid container item xs={12} lg={12} md={12} sm={12} mt={3}>
            <Grid item xs={12} md={12} sm={12} lg={12}>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <p style={{marginRight: 10, fontSize: 20}} >
                  Color: 
                </p>
                {productInfo !== ""
                  ? productInfo.color.map((element, index) => {
                      return (
                        <div
                          key={index}
                          onClick={() => setColor(element)}
                          style={
                            element === color
                              ? {
                                  color: "#fff",
                                  backgroundColor: element,
                                  width: 30,
                                  height: 30,
                                  borderRadius: "50%",
                                  border: "solid 1px black",
                                  cursor: "pointer",
                                  marginRight: 5,
                                }
                              : {
                                  color: "#fff",
                                  backgroundColor: element,
                                  width: 25,
                                  height: 25,
                                  borderRadius: "50%",
                                  cursor: "pointer",
                                  marginRight: 5,
                                }
                          }
                          value={element}
                        ></div>
                      );
                    })
                  : null}
              </div>
            </Grid>
            
            {/* <Grid item xs={12} md={12} sm={12} lg={12}>
              <FormControl sx={{ m: 1, minWidth: 200 }}>
                <InputLabel id="demo-simple-select-label">Color</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Color"
                  value={color}
                  onChange={(e) => setColor(e.target.value)}
                >
                  <MenuItem value="ALL">Choose your color</MenuItem>
                  {productInfo !== ""
                    ? productInfo.color.map((element, index) => {
                        return (
                          <MenuItem key={index} value={element}>
                            {element}
                          </MenuItem>
                        );
                      })
                    : null}
                </Select>
              </FormControl>
            </Grid> */}
            {/* <Grid item xs={12} md={12} sm={12} lg={12}>
              <FormControl sx={{ m: 1, minWidth: 200 }}>
                <InputLabel id="demo-simple-select-label">Size</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Size"
                  value={size}
                  onChange={(e) => setSize(e.target.value)}
                >
                  <MenuItem value="ALL">Choose your size</MenuItem>
                  {productInfo !== ""
                    ? productInfo.size.map((element, index) => {
                        return (
                          <MenuItem key={index} value={element}>
                            {element}
                          </MenuItem>
                        );
                      })
                    : null}
                </Select>
              </FormControl>
            </Grid> */}
          </Grid>
          <Grid item xs={12} md={12} sm={12} lg={12} mt={1}>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <p style={{marginRight: 10, fontSize: 20}} >
                  Size: 
                </p>
                {productInfo !== ""
                  ? productInfo.size.map((element, index) => {
                      return (
                        <div
                          key={index}
                          onClick={() => setSize(element)}
                          style={
                            element === size
                              ? {
                                  // color: "#fff",
                                  // backgroundColor: element,
                                  width: 45,
                                  height: 45,
                                  fontSize: 30,
                                  border: "solid 1px",
                                  cursor: "pointer",
                                  marginRight: 15,
                                  opacity: 1,
                                }
                              : {
                                  // color: "#fff",
                                  // backgroundColor: element,
                                  width: 30,
                                  height: 30,
                                  cursor: "pointer",
                                  marginRight: 10,
                                  fontSize: 20,
                                  border: "solid 1px",
                                  opacity: 0.7,
                                }
                          }
                          value={element}
                        >{element}</div>
                      );
                    })
                  : null}
              </div>
            </Grid>
          <Grid item xs={12} md={12} sm={12} lg={12} mt={1}>
            <Typography variant="h5" color="red">
              {" "}
              Price: {productInfo.buyPrice}$
            </Typography>
          </Grid>
          <Grid item xs={12} md={12} sm={12} lg={12}>
            <Stack
              direction="row"
              spacing={1}
              style={{ height: "40px", justifyContent: "center" }}
            >
              <IconButton
                onClick={reduceQuantity}
                color="primary"
                aria-label="remove a product"
                style={{ height: "40px", width: "40px" }}
              >
                <RemoveIcon />
              </IconButton>
              <Typography variant="h5">{quantity}</Typography>
              <IconButton
                onClick={addQuantity}
                color="primary"
                aria-label="add a product"
                style={{ height: "40px", width: "40px" }}
              >
                <AddIcon />
              </IconButton>
            </Stack>
          </Grid>

          <Grid item xs={12} md={12} sm={12} lg={12}>
            <Button
              onClick={() => addToCard(productInfo)}
              color="error"
              variant="contained"
            >
              Add to Card
            </Button>
          </Grid>
          <Grid container textAlign="justify" mt={3} paddingX={4}>
            <Grid item xs={12} md={12} sm={12} lg={12}>
              <Typography
                variant="h5"
                fontWeight="bold"
                style={{ color: "#8f5741" }}
              >
                Description
              </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12}>
              <Typography>{productInfo.description}</Typography>
            </Grid>
          </Grid>
          {/* <Grid container textAlign="justify" mt={3} paddingX={4}>
            <Grid item xs={12} md={12} sm={12} lg={12} >
              <Typography variant="h5" fontWeight="bold" style={{ "color": "#8f5741"}}>
                Feedback
              </Typography>
            </Grid>
            <Grid item xs={12} md={12} sm={12} lg={12}>
            {productInfo!=="" ? productInfo.reviews.map((review,index) => {
              <div key={index}>
                <p >{review.fullName}</p>
                <p>{review.email}</p>
                <p >{review.description}</p>
              </div>
            }):<p>No reviews</p>}
            </Grid>
          </Grid> */}
        </Grid>
      </Grid>

      <Snackbar
        open={isOpenAlert}
        autoHideDuration={3000}
        onClose={handleCloseAlert}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseAlert}
          severity={statusModal}
          sx={{ width: "100%" }}
        >
          {noidungAlertValid}
        </Alert>
      </Snackbar>
    </Grid>
  );
}
export default ProductData;
