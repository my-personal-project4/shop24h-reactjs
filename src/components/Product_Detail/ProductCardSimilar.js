import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
// import { useEffect } from "react";

function ProductCardSimilar({ product }) {
  const navigate = useNavigate();
  const hrefProductDetail = `/products/${product._id}`;
  const onCardClick = () => {
    navigate(hrefProductDetail);
  };
  // console.log(product.color);
  return (
      <Card sx={{ maxWidth: 345, height: "100%",  }}>
        <CardActionArea >
          <CardContent onClick={onCardClick}>
            <img
              src={product.imageUrl}
              alt={product.name}
              style={{ maxWidth: "100%", width: "200px", height: "200px", objectFit: "fill", margin: "auto" }}
            />
            <Typography gutterBottom variant="h6" component="div">
              {product.name}
            </Typography>
            {/* <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Color</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
              >
                {product.color.map((element, index) =>{
                    return(
                        <MenuItem key={index} value={element}>{element}</MenuItem>
                    )
                })}
              </Select>
            </FormControl> */}
            <Typography variant="h5" color="red">
              {product.buyPrice} $
            </Typography>
            
          </CardContent>
        </CardActionArea>
      </Card>
    
  );
}
export default ProductCardSimilar;
