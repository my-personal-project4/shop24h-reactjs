import { Grid, Typography } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProductsSimilar } from "../../Redux/Actions/GetAllProductAction";
// import Swiper core and required modules
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import ProductCardSimilar from "./ProductCardSimilar";

function SimilarProducts() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const dispatch = useDispatch();
  const { products } = useSelector((reduxData) => reduxData.getProductsReducer);

  useEffect(() => {
    dispatch(getProductsSimilar());
  }, []);
  return (
    <>
      <div style={{width: "100%", height:"3px", backgroundColor:"green",marginTop: "80px"}}></div>
      <Grid container textAlign="center" spacing={2} mt={2}>
        <Grid item xs={12} md={12} sm={12} lg={12}>
          <Typography
            variant="h5"
            fontWeight="bold"
            textAlign="center"
            style={{ color: "#8f5741" }}
          >
            Similar Products
          </Typography>
        </Grid>
        <Swiper
          // install Swiper modules
          modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
          loop={true}
          spaceBetween={10}
          slidesPerView={4}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          // navigation
          // pagination={{ clickable: true }}
          // scrollbar={{ draggable: true }}
          breakpoints={{
            0: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            480: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 10,
            },
            1024: {
              slidesPerView: 4,
              spaceBetween: 10,
            },
          }}
        >
          {products.map((product, index) => {
            return (
              <SwiperSlide key={index}>
                <ProductCardSimilar product={product} />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </Grid>
    </>
  );
}
export default SimilarProducts;
