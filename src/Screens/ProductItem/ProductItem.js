import BreadCrumb from '../../Components/BreadCrumb/BreadCrumb';
import { Container } from "@mui/material";
import { useEffect } from "react";
import ProductDetail from '../../Components/Product_Detail/ProductDetail';

function ProductItem (productInfo) {
  const breadcrumbArray = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "All Product",
      url: "/products",
    },
  ];
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])
  return (
    <div >
      <Container>
        <BreadCrumb breadcrumbArray={breadcrumbArray} pageName={productInfo.name} />
        <ProductDetail />
      </Container>
    </div>
  );
}

export default ProductItem;