import React from "react";
import { Slide } from "../../Components/Slider/Slide";
import { CategoryCard } from "../../Components/categoryCard/CategoryCard";
import Product1s from '../../Components/Products/Products'
// import { listProductAction } from "../../Redux/Actions/Action";
export function Home() {
  return (
    <>
      <Slide></Slide>
      <CategoryCard></CategoryCard>
      <Product1s></Product1s>
    </>
  );
}
