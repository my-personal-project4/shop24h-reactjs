export const NAME_CHANGE = "NAME_CHANGE";
export const MIN_PRICE = "MIN_PRICE";
export const MAX_PRICE = "MAX_PRICE";
export const BRAND_CHANGE = "BRAND_CHANGE";
export const COLOR_CHANGE = "COLOR_CHANGE";
export const SIZE_CHANGE = "SIZE_CHANGE";