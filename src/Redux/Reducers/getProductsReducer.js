import { GET_ALL_PRODUCT, CHANGE_PAGE_PAGINATION, GET_PRODUCT, GET_ALL_PRODUCT_LIMIT, GET_PRODUCT_FILTER} from "../Constants/GetAllProductConstant";

const limit = 8;

const initialState = {
    currentPage: 1,
    products: [],
    noPage: 0,
    productInfo: "",
}

export default function getProductsReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGE_PAGE_PAGINATION:
            return {
                ...state,
                currentPage: action.payload
            }
        case GET_PRODUCT:
            return {
                ...state,
                productInfo: action.payload
            }
        case GET_ALL_PRODUCT_LIMIT:
            return {
                ...state,
                products: action.payload
            }
        case GET_PRODUCT_FILTER:
            return {
                ...state,
                products: action.payload.product,
                noPage: Math.ceil(action.payload.total/action.payload.limit)
            }
        default:
            return state;
    }
}