import { PRODUCT_TYPE } from "../Constants/GetProductTypeConstant";

const initialState = {
  productType: [],
}

export default function getTypeReducer(state = initialState, action) {
  switch (action.type) {
      case PRODUCT_TYPE:
          return {
              ...state,
              productType: action.payload
          }
      default:
          return state;
  }
}