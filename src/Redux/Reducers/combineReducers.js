import { combineReducers } from "redux";

import {
  themeReducer,
} from "./reducer";
import userLoginReducer from './userLoginReducer';
import getProductsReducer from "./getProductsReducer";
import filterValueReducer from "./filterValueReducer";
import getTypeReducer from "./getProductType"
import cardReducer from "./CardReducer";
import checkOutReducer from "./CheckOutReducer";
import getOrderReducers from "./getOrdersReducer";
import getReviewsReducer from "./ReviewReducer";

export const reducers = combineReducers({
  theme: themeReducer,
  userLoginReducer,
  getProductsReducer,
  filterValueReducer,
  getTypeReducer,
  cardReducer,
  checkOutReducer,
  getOrderReducers,
  getReviewsReducer,
});
