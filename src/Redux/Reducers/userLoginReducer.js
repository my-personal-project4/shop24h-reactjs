import { USER_LOGIN, EMAIL_LOGIN } from "../Constants/UserLoginConstant";

const initialState = {
  user: null,
  emailLogin: null,
}

const userLoginReducer = (state= initialState, action) => {
  switch(action.type){
    case USER_LOGIN:
            return {
                ...state,
                user: action.payload
            }
    case EMAIL_LOGIN:
            return {
                ...state,
                emailLogin: action.payload
            }
    default:
        return state;
  }
} 

export default userLoginReducer;