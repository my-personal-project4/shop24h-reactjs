import { NAME_CHANGE, MIN_PRICE, MAX_PRICE , BRAND_CHANGE, COLOR_CHANGE, SIZE_CHANGE} from "../Constants/FilterValue";

const initialState = {
  productName: "",
  minPrice: "",
  maxPrice: "",
  color: "ALL",
  size: "ALL",
  type: "ALL",
}

const filterValueReducer = (state= initialState, action) => {
  switch(action.type){
    case NAME_CHANGE:
            return {
                ...state,
                productName: action.payload
            }
    case MIN_PRICE:
            return {
                ...state,
                minPrice: action.payload
            }
    case MAX_PRICE:
            return {
                ...state,
                maxPrice: action.payload
            }
    case BRAND_CHANGE:
            return {
                ...state,
                type: action.payload
            }
    case COLOR_CHANGE:
            return {
                ...state,
                color: action.payload
            }
    case SIZE_CHANGE:
            return {
                ...state,
                size: action.payload
            }
    default:
        return state;
  }
} 

export default filterValueReducer;