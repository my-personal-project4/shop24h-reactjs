import { CHANGE_PAGINATION_ORDERS, CLEAR_ALL_ORDERS, GET_ALL_ORDERS } from "../Constants/getOrdersConstant";



const initialState = {
  currentPage: 1,
  orders: [],
  noPage: 0,
  
}

export default function getOrderReducers(state = initialState, action) {
  switch (action.type) {
      
        case GET_ALL_ORDERS:
          return {
              ...state,
              orders: action.payload.order,
              noPage: Math.ceil(action.payload.total/action.payload.limit)
              // pending: false
          }
     
        case CHANGE_PAGINATION_ORDERS:
          return {
              ...state,
              currentPage: action.payload
          }
        case CLEAR_ALL_ORDERS:
          return {
              ...state,
              orders: []
          }
        
      default:
          return state;
  }
}