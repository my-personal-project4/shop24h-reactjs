import { FULL_NAME_CHANGE,
  EMAIL_CHANGE,
  PHONE_CHANGE,
  ADDRESS_CHANGE,
  COUNTRY_CHANGE,
  CITY_CHANGE,
  MESSAGE_CHANGE, 
  CREATE_ORDER,
  GET_ALL_CITY,
  GET_ALL_DISTRICT} from "../Constants/CheckOutInfo";

const initialState = {
  fullname: "",
  email: "",
  phoneNumber: "",
  address: "",
  country: "ALL",
  city: "ALL",
  note: "",
  orders: {},
  allCity: [],
  allDistrict: [],
}

export default function checkOutReducer(state = initialState, action) {
  switch (action.type) {
    case "GET_USER_INFOMATION":
      const user = action.value;

      if (user.displayName) {
        state.fullname = user.displayName;
      };
      if (user.email) {
        state.email = user.email;
      };
      if (user.phoneNumber) {
        state.phoneNumber = user.phoneNumber;
      };
      if (user.address) {
        state.address = user.address;
      };
      return {
        ...state,
      };
      case FULL_NAME_CHANGE:
          return {
              ...state,
              fullname: action.payload
          }
      case EMAIL_CHANGE:
          return {
              ...state,
              email: action.payload
          }
      case PHONE_CHANGE:
          return {
              ...state,
              phoneNumber: action.payload
          }
      case ADDRESS_CHANGE:
          return {
              ...state,
              address: action.payload
          }
      case COUNTRY_CHANGE:
          return {
              ...state,
              country: action.payload
          }
      case CITY_CHANGE:
          return {
              ...state,
              city: action.payload
          }
      case MESSAGE_CHANGE:
          return {
              ...state,
              note: action.payload
          }
      case CREATE_ORDER:
          return {
              ...state,
              orders: action.payload
          }
      case GET_ALL_CITY:
          return {
              ...state,
              allCity: action.payload
          }
      case GET_ALL_DISTRICT:
          return {
              ...state,
              allDistrict: action.payload
          }
      default:
          return state;
  }
}