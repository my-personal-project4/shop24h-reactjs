import { CREATE_REVIEW, GET_ALL_REVIEWS } from "../Constants/ReviewConstant";


const initialState = {
  createReviews: [],
  reviews: [],

}

export default function getReviewsReducer(state = initialState, action) {
  switch (action.type) {
      case CREATE_REVIEW:
          return {
              ...state,
              createReviews: action.payload
          }
      case GET_ALL_REVIEWS:
          return {
              ...state,
              reviews: action.payload
          }
      default:
          return state;
  }
}