import { ADD_ITEM, CLEAR_STORAGE, DECREASE_ITEM, DELETE_ITEM, INCREASE_ITEM} from "../Constants/CartConstant";

let initialState = {
  cartItem: [],
  itemQuantity: 0,
  

}
// if (JSON.parse(localStorage.getItem("cartItem"))) {
//   initialState = {
//       itemQuantity: JSON.parse(localStorage.getItem("itemQuantity")),
//       cartItem: JSON.parse(localStorage.getItem("cartItem")),
     
//   };
// }

export default function cardReducer(state = initialState, action) {
  switch (action.type) {
      case ADD_ITEM:
        let isNewProduct = true;
        const addQuantityForProduct = (productIndex, quantity) => {
            state.cartItem[productIndex].quantity += quantity
        }
        
        state.cartItem.forEach((item, index) => {
            const itemId = item._id;
            if (itemId === action.payload.itemInfo._id) {
                addQuantityForProduct(index, action.payload.quantity)
                isNewProduct = false;
            }
        });
        if (isNewProduct) {
            return {
                ...state,
                itemQuantity: (state.itemQuantity + action.payload.quantity),
                cartItem: [...state.cartItem, action.payload.itemInfo]
            };
        }
        return {
            ...state,
            itemQuantity: (state.itemQuantity + action.payload.quantity)
        }

      case INCREASE_ITEM:
        let isNewProduct1 = true;
        const addQuantityForProduct1 = (productIndex, quantity) => {
            state.cartItem[productIndex].quantity += quantity
        }
        state.cartItem.forEach((item, index) => {
            const itemId = item._id;
            if (itemId === action.payload.itemInfo._id) {
                addQuantityForProduct1(index, action.payload.quantity)
                isNewProduct1 = false;
            }
        });
        if (isNewProduct1) {
            return {
                ...state,
                itemQuantity: (state.itemQuantity + action.payload.quantity),
                cartItem: [...state.cartItem, action.payload.itemInfo]
            };
        }
        return {
            ...state,
            itemQuantity: (state.itemQuantity + action.payload.quantity)
        }
      
        case DELETE_ITEM:
          const quantityDelete = action.payload.productInfo.quantity;
          state.cartItem.splice(action.payload.productIndex, 1);
          return {
              ...state,
              itemQuantity: (state.itemQuantity - quantityDelete)
          }
        case DECREASE_ITEM:
          if (state.cartItem[action.payload.itemIndex].quantity > 1) {
              state.cartItem[action.payload.itemIndex].quantity -= 1;
              return {
                  ...state,
                  itemQuantity: (state.itemQuantity - 1)
              }
          }
          return state;
        case CLEAR_STORAGE:
              return {
                  ...state,
                  itemQuantity: 0,
                  cartItem: []
              }
          
      default:
          return state;
  }
}