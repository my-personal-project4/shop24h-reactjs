
import {CREATE_REVIEW, GET_ALL_REVIEWS} from '../Constants/ReviewConstant'

export const createReview= (productReview, stars, feedback, setOpenAlert,setStatusModal,setNoidungAlertValid,dataReview, setOpenModalReview) => async dispatch => {

  var requestOptions = {
    method: 'POST',
    body: JSON.stringify({
      fullName: dataReview.fullName,
      email: dataReview.email,
      rating: stars,
      description: feedback,
    }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
      const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/products/${productReview}/reviews`, requestOptions
        )
        const data = await response.json();
        setOpenAlert(true);
        setStatusModal('success');
        setNoidungAlertValid('Give review Success!')
        setOpenModalReview(false);
        // console.log(data);
        return dispatch({
            type: CREATE_REVIEW,
            payload: data.Reviews
        });
  } catch (err) {
      
  }
};


export const getReviews= (productid) => async dispatch => {

  var requestOptions = {
      method: 'GET',
      redirect: 'follow'
  }

  try {
      const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/products/${productid}/reviews`, requestOptions
        )
        const data = await response.json();
        // console.log(data);
        return dispatch({
            type: GET_ALL_REVIEWS,
            payload: data.Review
        });
  } catch (err) {
      
  }
};