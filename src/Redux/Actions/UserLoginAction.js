import { USER_LOGIN, EMAIL_LOGIN } from "../Constants/UserLoginConstant";

export function userLoginAction(value) {
  return {
    type: USER_LOGIN,
    payload: value
  }
}
export function emailLoginAction(value) {
  return {
    type: EMAIL_LOGIN,
    payload: value
  }
}