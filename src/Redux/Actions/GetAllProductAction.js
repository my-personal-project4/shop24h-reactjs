import {
 CHANGE_PAGE_PAGINATION, GET_PRODUCT, GET_ALL_PRODUCT_LIMIT, GET_PRODUCT_FILTER
} from "../Constants/GetAllProductConstant";

export function changePagePagination(value) {
  return {
      type: CHANGE_PAGE_PAGINATION,
      payload: value
  };
}

export const getProductsLimit = (limit) => async dispatch => {

  var requestOptions = {
      method: 'GET',
      redirect: 'follow'
  }

  try {
      const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/products/?limit=${limit}`, requestOptions
        )
        const data = await response.json();

        return dispatch({
            type: GET_ALL_PRODUCT_LIMIT,
            payload: data.product
        });
  } catch (err) {
      
  }
};
export const getProductsSimilar = () => async dispatch => {

  var requestOptions = {
      method: 'GET',
      redirect: 'follow'
  }

  try {
      const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/products-random`, requestOptions
        )
        const data = await response.json();

        return dispatch({
            type: GET_ALL_PRODUCT_LIMIT,
            payload: data.product
        });
  } catch (err) {
      
  }
};

export const getAllProductFilter= (productName,minPrice, maxPrice,type,color,size,currentPage,limit ) => async dispatch => {
  // console.log(checked.join())
  var requestOptions = {
      method: 'GET',
      redirect: 'follow'
  }

  try {
      if(color === "ALL" && size === "ALL" && type === "ALL") {
        const response = await fetch(
          `${process.env.REACT_APP_BACKEND_DOMAIN}/products/?productName=${productName}&minPrice=${minPrice}&maxPrice=${maxPrice}&&page=${currentPage}&&limit=${limit}`, requestOptions
          )
          const data = await response.json();
          return dispatch({
            type: GET_PRODUCT_FILTER,
            payload: {
              product:data.product,
              limit: data.limit,
              total: data.total
            }
          });
      } 
      else if(color !== "ALL" || size !== "ALL" || type !== "ALL") {
        const response = await fetch(
          `${process.env.REACT_APP_BACKEND_DOMAIN}/products/?productName=${productName}&minPrice=${minPrice}&maxPrice=${maxPrice}&color=${color}&size=${size}&&page=${currentPage}&&limit=${limit}&&type=${type}`, requestOptions
          )
          const data = await response.json();
          return dispatch({
            type: GET_PRODUCT_FILTER,
            payload: {
              product:data.product,
              limit: data.limit,
              total: data.total
            }
          });
      }
  } catch (err) {
      
  }
};

export const getProduct = (id) => async dispatch => {
  var requestOptions = {
      method: 'GET',
      redirect: 'follow'
  }

  try {
      const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/products/${id}`, requestOptions
        )
      const data = await response.json();
      // console.log(data.Product)
      return dispatch({
          type: GET_PRODUCT,
          payload: data.Product,
      });
  } catch (err) {
      
  }
};