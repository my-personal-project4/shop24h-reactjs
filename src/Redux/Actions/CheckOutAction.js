import {
  FULL_NAME_CHANGE,
  EMAIL_CHANGE,
  PHONE_CHANGE,
  ADDRESS_CHANGE,
  COUNTRY_CHANGE,
  CITY_CHANGE,
  MESSAGE_CHANGE,
  CREATE_ORDER,
  GET_ALL_CITY,
  GET_ALL_DISTRICT,
} from "../Constants/CheckOutInfo";

export function fullnameChange(value) {
  return {
    type: FULL_NAME_CHANGE,
    payload: value,
  };
}
export function messageChange(value) {
  return {
    type: MESSAGE_CHANGE,
    payload: value,
  };
}
export function emailChange(value) {
  return {
    type: EMAIL_CHANGE,
    payload: value,
  };
}
export function phoneChange(value) {
  return {
    type: PHONE_CHANGE,
    payload: value,
  };
}
export function addressChange(value) {
  return {
    type: ADDRESS_CHANGE,
    payload: value,
  };
}
export function countryChange(value) {
  return {
    type: COUNTRY_CHANGE,
    payload: value,
  };
}
export function cityChange(value) {
  return {
    type: CITY_CHANGE,
    payload: value,
  };
}

export const getAllCity = () => async (dispatch) => {
  var requestCity = {
    method: "GET",
    redirect: "follow",
  };

  try {
    // const response = await fetch(
    //   `https://vapi.vnappmob.com/api/province/`, requestCity
    //   )
    const response = await fetch(
      `https://countriesnow.space/api/v0.1/countries`,
      requestCity
    );
    const data = await response.json();
    return dispatch({
      type: GET_ALL_CITY,
      payload: data.data,
    });
  } catch (err) {
    alert(err);
  }
};
// export const getAllDistrict = (id) => async dispatch => {
//   var requestCity = {
//     method: 'GET',
//     redirect: 'follow'
//   }

//   try {
//     const response = await fetch(
//       `https://vapi.vnappmob.com/api/province/district/${id}`, requestCity
//       )
//       const data = await response.json();
//       // console.log(data)
//       return dispatch({
//           type: GET_ALL_DISTRICT,
//           payload: data.results
//       });
//   } catch (err) {
//     alert(err);
//   }
// };

export const createOrder =
  (vDataForm, cartItem, totalPrice) => async (dispatch) => {
    // console.log(cartItem);
    var requestCreateCustomer = {
      method: "POST",
      body: JSON.stringify({
        fullName: vDataForm.fullName,
        phone: vDataForm.phoneNumber,
        email: vDataForm.email,
        address: vDataForm.address,
        city: vDataForm.city,
        country: vDataForm.country,
        message: vDataForm.note,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };

    var requestCustomer = {
      method: "GET",
      redirect: "follow",
    };

    try {
      const responseCustomer = await fetch(
        `http://localhost:8000/customers?phoneNumber=${vDataForm.phoneNumber}`,
        requestCustomer
      );
      const customerResult = await responseCustomer.json();
      // console.log(customerResult);
      if (customerResult.Customers.length === 0) {
        const responseCreateCustomer = await fetch(
          `http://localhost:8000/customers`,
          requestCreateCustomer
        );
        const customerCreateResult = await responseCreateCustomer.json();
        // console.log(customerCreateResult)
        if (customerCreateResult) {
          const customerId = customerCreateResult.newCustomer._id;
          // console.log(customerId);
          let requestCreateOrder = {
            method: "POST",
            body: JSON.stringify({
              // orderDetail: cartItem,
              fullName: customerCreateResult.newCustomer.fullName,
              phone: customerCreateResult.newCustomer.phone,
              email: customerCreateResult.newCustomer.email,
              address: customerCreateResult.newCustomer.address,
              city: customerCreateResult.newCustomer.city,
              country: customerCreateResult.newCustomer.country,
              cost: totalPrice,
              note: customerCreateResult.newCustomer.message,
              // orderDetails: cartItem
            }),
            headers: {
              "Content-type": "application/json; charset=UTF-8",
            },
          };
          const responseCreateOrder = await fetch(
            `http://localhost:8000/customers/${customerId}/orders`,
            requestCreateOrder
          );
          const orderResult = await responseCreateOrder.json();
          console.log(orderResult);
          if (orderResult) {
            const orderId = orderResult.order._id;
            // console.log(cartItem)
            // console.log(orderId);
            cartItem.map(async (item) => {
              var vOrderDetail = {
                product: item._id,
                quantity: item.quantity,
              };
              // console.log(vOrderDetail)
              let orderDetailRequest = {
                method: "POST",
                body: JSON.stringify(vOrderDetail),
                headers: {
                  "Content-type": "application/json; charset=UTF-8",
                },
              };
              const responseCreateDetail = await fetch(
                `http://localhost:8000/orders/${orderId}/orderDetails`,
                orderDetailRequest
              );
              const orderDetailResult = await responseCreateDetail.json();
              console.log(orderDetailResult);
            });
          }
          return dispatch({
            type: CREATE_ORDER,
            payload: orderResult,
          });
        }
      } else {
        const customerId = customerResult.Customers[0]._id;
        // console.log(customerId);
        let requestCreateOrder = {
          method: "POST",
          body: JSON.stringify({
            // orderDetail: cartItem,
            fullName: customerResult.Customers[0].fullName,
            phone: customerResult.Customers[0].phone,
            email: customerResult.Customers[0].email,
            address: customerResult.Customers[0].address,
            city: customerResult.Customers[0].city,
            country: customerResult.Customers[0].country,
            cost: totalPrice,
            note: customerResult.Customers[0].message,
          }),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
        };
        const responseCreateOrder = await fetch(
          `http://localhost:8000/customers/${customerId}/orders`,
          requestCreateOrder
        );
        const orderResult = await responseCreateOrder.json();

        if (orderResult) {
          const orderId = orderResult.order._id;
          // console.log(orderId);
          cartItem.map(async (item) => {
            var vOrderDetail = {
              product: item._id,
              quantity: item.quantity,
            };
            let orderDetailRequest = {
              method: "POST",
              body: JSON.stringify(vOrderDetail),
              headers: {
                "Content-type": "application/json; charset=UTF-8",
              },
            };
            const orderDetailResult = await fetch(
              `http://localhost:8000/orders/${orderId}/orderDetails`,
              orderDetailRequest
            );
            // console.log(orderDetailResult);
          });
        }
        return dispatch({
          type: CREATE_ORDER,
          payload: orderResult,
        });
      }
    } catch (err) {
      alert(err);
    }
  };
