
import { PRODUCT_TYPE } from "../Constants/GetProductTypeConstant";


export const getType= () => async dispatch => {

  var requestOptions = {
      method: 'GET',
      redirect: 'follow'
  }

  try {
      const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/productTypes/`, requestOptions
        )
        const data = await response.json();
        // console.log(data);
        return dispatch({
            type: PRODUCT_TYPE,
            payload: data.ProductTypes
        });
  } catch (err) {
      
  }
};