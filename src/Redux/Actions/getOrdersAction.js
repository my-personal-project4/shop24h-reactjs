import {GET_ALL_ORDERS, CHANGE_PAGINATION_ORDERS, CLEAR_ALL_ORDERS} from '../Constants/getOrdersConstant'


export function changePagePaginationOrder(value) {
  return {
      type: CHANGE_PAGINATION_ORDERS,
      payload: value
  };
}

export function clearStorage() {
  return {
    type: CLEAR_ALL_ORDERS
  }
}


export const getOrders = (currentPage,limit) => async dispatch => {
  var requestOptions = {
      method: 'GET',
      redirect: 'follow',
      headers: {
        Authorization: "Bearer " + localStorage.getItem("@token"),
      },

  }


  try {
      const response = await fetch(
        `http://localhost:8000/customers-check?page=${currentPage}&&limit=${limit}`, requestOptions
      );

      const data = await response.json();
      // console.log(data);
      // return dispatch({
      //     type: GET_ALL_ORDERS,
      //     payload: {
      //       order:data.customer[0].orders,
      //       limit: data.limit,
      //       total: data.total
      //     },
      // });
      return dispatch({
          type: GET_ALL_ORDERS,
          payload: {
            order:data.customer,
            limit: data.limit,
            total: data.total
          },
      });
  } catch (err) {
      
  }
};