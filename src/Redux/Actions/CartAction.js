import { ADD_ITEM, DECREASE_ITEM, DELETE_ITEM, INCREASE_ITEM,CLEAR_STORAGE} from "../Constants/CartConstant";

export function addCartItem(value,quantity) {
  return {
      type: ADD_ITEM,
      payload:{
        itemInfo: { ...value, quantity: quantity },
        quantity
      }
  };
}
export function increaseItem1(value,quantity) {
  return {
      type: INCREASE_ITEM,
      payload:{
        itemInfo: value,
        quantity
      }
  };
}
export function deleteCartItem(productInfo, productIndex) {
  return {
      type: DELETE_ITEM,
      payload:{
        productInfo: productInfo,
        productIndex: productIndex
      }
  };
}


export function decreaseItem1(item, itemIndex) {
  return {
      type: DECREASE_ITEM,
      payload: {
        item: item,
        itemIndex: itemIndex
      }
  };
}

export function clearStorage() {
  return {
    type: CLEAR_STORAGE
  }
}
