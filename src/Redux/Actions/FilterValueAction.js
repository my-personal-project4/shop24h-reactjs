import { NAME_CHANGE, MIN_PRICE, MAX_PRICE,BRAND_CHANGE, COLOR_CHANGE, SIZE_CHANGE } from "../Constants/FilterValue";

export function nameChange(value) {
  return {
      type: NAME_CHANGE,
      payload: value
  };
}

export function minPriceChange(value) {
  return {
      type: MIN_PRICE,
      payload: value
  };
}

export function maxPriceChange(value) {
  return {
      type: MAX_PRICE,
      payload: value
  };
}
export function brandChange(value) {
  return {
      type: BRAND_CHANGE,
      payload: value
  };
}
export function colorChange(value) {
  return {
      type: COLOR_CHANGE,
      payload: value
  };
}
export function sizeChange(value) {
  return {
      type: SIZE_CHANGE,
      payload: value
  };
}