// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCJaO8HfR9VjjF6GUt2gefVNrUarzQo_q8",
  authDomain: "shop24h-reactjs.firebaseapp.com",
  projectId: "shop24h-reactjs",
  storageBucket: "shop24h-reactjs.appspot.com",
  messagingSenderId: "962451137059",
  appId: "1:962451137059:web:6c94af5c3a602ec24e0796"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

export default auth;